import dayjs from "dayjs";

/**
 * @desc 判断 HK 手机号
 * @param {string} mobile
 */
export function isHKMobile(mobile) {
  return /^(5|6|8|9)\d{7}$/.test(mobile);
}

/**
 * @desc 判断 大陆 手机号
 * @param {string} mobile
 */
export function isMainlandMobile(mobile) {
  return /^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(166)|(17[0,1,3,5,6,7,8])|(18[0-9])|(19[8|9]))\d{8}$/.test(
    mobile
  );
}

export function getCallPrefix(mobile) {
  if (isHKMobile(mobile)) {
    return 852;
  } else if (isMainlandMobile(mobile)) {
    return 86;
  } else {
    throw new Error("找不到区号");
  }
}

export function fmtDate(time, format = "YYYY年MM月DD日") {
  return dayjs(time).format(format);
}
