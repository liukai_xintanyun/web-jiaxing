import Vue from "vue";
import Vuex from "vuex";
import { getCurrency, getUserAsset, getTodayPrice } from "./api";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    cashRatio: 1,
    carbonRatio: 1,
    // 未转化过的资产
    cashAsset: 0,
    freezeCashAsset: 0,
    carbonAsset: 0,
    freezeCarbonAsset: 0,
    todayPrice: 0
  },
  mutations: {
    setCurrency(state, payload) {
      state.carbonRatio = payload.carbonRatio;
      state.cashRatio = payload.cashRatio;
    },
    setAssets(state, payload) {
      state.cashAsset = payload.cashAsset;
      state.carbonAsset = payload.carbonAsset;
      state.freezeCashAsset = payload.freezeCashAsset;
      state.freezeCashAsset = payload.freezeCashAsset;
    },
    setPrice(state, payload) {
      state.todayPrice = payload;
    }
  },
  actions: {
    async setCurrency({ commit }) {
      let {
        data: { data }
      } = await getCurrency();
      const cashRatio = 10 ** data.cash.places;
      const carbonRatio = 10 ** data.carbon.places;
      const payload = {
        cashRatio,
        carbonRatio
      };
      localStorage.setItem("CURRENCY", JSON.stringify(payload));
      commit("setCurrency", payload);
    },
    async setAssets({ commit }) {
      let {
        data: { data }
      } = await getUserAsset();
      commit("setAssets", {
        cashAsset: data.cash.asset,
        freezeCashAsset: data.cash.freeze,
        carbonAsset: data.carbon.asset,
        freezeCarbonAsset: data.carbon.freeze
      });
    },
    async setPrice({ commit }) {
      let {
        data: { data }
      } = await getTodayPrice();
      commit("setPrice", Number(data));
    }
  },
  getters: {
    realCarbonAsset: state => state.carbonAsset / state.carbonRatio,
    realFreezeCarbonAsset: state => state.freezeCarbonAsset / state.carbonRatio,
    realCashAsset: state => state.cashAsset / state.cashRatio,
    realFreezeCashAsset: state => state.freezeCashAsset / state.cashRatio,
    realTodayPrice: state => state.todayPrice / state.cashRatio
  }
});
