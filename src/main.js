import Vue from "vue";
import axios from "axios";
import { List } from "vant";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { Toast } from "vant";
import { autoLogin } from "./login.service"
import "vant/lib/index.css";

Vue.use(List);

if (process.env.NODE_ENV === "development") {
  // axios.defaults.baseURL = "/api";
  axios.defaults.baseURL = "https://test.xintanyun.com";
} else {
  axios.defaults.baseURL = "https://www.xintanyun.com"; //线上
}

axios.interceptors.request.use(
  function(config) {
    config.headers["device"] = "h5";
    config.headers["platform"] = 0;
    try {
      let user = localStorage.getItem("USER");
      let result = JSON.parse(user);
      if (!config.url.includes("/user/registry")) {
        config.headers["token"] = result.token;
      } else {
        delete config.headers["token"];
      }
    } catch (error) {
      console.log(error);
    }
    return config;
  },
  function(error) {
    return Promise.reject(error);
  }
);
axios.interceptors.response.use(
  function(response) {
    // 刷新 token & update localstorage
    let { headers } = response;
    let token = headers["refresh_token"];
    if (token) {
      axios.defaults.headers["token"] = token;
      try {
        let userData = JSON.parse(localStorage.getItem("USER"));
        localStorage.setItem({
          ...userData,
          token
        });
      } catch (error) {
        Toast(error.message);
      }
    }

    // 登录拦截
    let { code } = response.data;
    if (code === 401) {
      localStorage.removeItem("USER");
      Toast.fail({
        message: "未登录, 前往登录",
        duration: 1000,
        onClose: () => {
          router.replace("/login");
        }
      });
      return Promise.reject("登录过期或者未登录");
    }
    return response;
  },
  function(error) {
    return Promise.reject(error);
  }
);

Vue.config.productionTip = false;

// Currency

try {
  let res = JSON.parse(localStorage.getItem("CURRENCY"));
  if (res) [store.commit("setCurrency", res)];
} catch (error) {
  console.log(error);
}

router.beforeEach((to, from, next) => {
  const user = localStorage.getItem("USER");
  console.log(user)
  if (to.name !== "login" && !user) {
    autoLogin().then(({ data }) => {
      console.log(data)
      localStorage.setItem('USER', JSON.stringify(data.data))
      store.dispatch("setPrice");
      store.dispatch("setAssets");
      next("/")
    }).catch(err => {
      next("/login");
    })
  } else {
    next();
  }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
