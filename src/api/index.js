import axios from "axios";

export function getRecentTradePrice(days) {
  return axios.get("/core/tradePrice/lastDayOfTradePrice/", {
    params: { days }
  });
}

export function getTodayPrice(days) {
  return axios.post("/core/tradePrice/today/", {
    days
  });
}

export function getCurrency() {
  return axios.get("/core/currency/getCurrencies/").then(res => {
    if (res.data.code === "OK") {
      let carbon = res.data.data.find(e => e.assetType === 0);
      let cash = res.data.data.find(e => e.assetType === 1);
      res.data.data = {
        carbon,
        cash
      };
    }
    return res;
  });
}

export function getUserAsset() {
  return axios.post("/core/asset/userAsset/").then(res => {
    if (res.data.code === "OK") {
      let carbon = res.data.data.filter( e => e.assetType !== 1).reduce((acc, cur) => {
        return {
          asset: acc.asset + cur.asset,
          freeze: acc.freeze + cur.freeze,
        }
      }, {
        asset: 0,
        freeze: 0,
      });

      let cash = res.data.data.find(e => e.assetType === 1);
      res.data.data = {
        carbon,
        cash
      };
    }
    return res;
  });
}

export function loginFromWeb(payload) {
  return axios.post("user/registry/login/", payload);
}

export function sendCode(payload) {
  return axios.post("user/registry/sendVerifyCode/", payload);
}

/**
 * @desc 获取订单列表
 */
export function getOrderList(payload) {
  return axios.post("core/order/orders", payload);
}

/**
 * @desc 取消订单
 */
export function cancelOrder(id) {
  return axios.post(`core/order/cancelOrder?orderId=${id}`);
}

/**
 * @desc 获取碳账户流水相关
 * @param {{ pageNum: Number, pageSize: Number }} payload
 */
export function getAssetHistory(payload) {
  return axios.post("core/assetHistory/getAssetHistory", payload);
}

/**
 * @desc 下单
 * @param {{ amount: Number, price: Number, side: 0 | 1}} payload // 买入 | 卖出
 */
export function order(payload) {
  return axios.post("core/order/placeOrder", payload);
}
