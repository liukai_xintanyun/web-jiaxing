import queryString from "query-string";
import axios from "axios";


export function autoLogin() {
  let query = queryString.parse(location.search)
  let {
    mobile,
    code,
    appKey,
    timestamp
  } = query
  if (mobile && code && appKey && timestamp) {
    // 登录换取 token
    return axios.post("/user/registry/replaceToken", {
      mobile: mobile.replace("/", ""),
      code: code.replace("/", ""),
      appKey: appKey.replace("/", ""),
      timestamp: timestamp.replace("/", "")
    }).then(res => {
      return res
    })
  } else {
    return Promise.reject("")
  }
}