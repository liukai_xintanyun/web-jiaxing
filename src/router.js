import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Login from "./views/Login.vue";
import PendOrder from "./views/PendOrder.vue";
import TransactionRecord from "./views/TransactionRecord.vue";
import Transaction from "./views/Transaction";
import Billing from "./views/Billing";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/login",
      name: "login",
      component: Login
    },
    {
      path: "/PendOrder",
      name: "PendOrder",
      component: PendOrder
    },
    {
      path: "/TransactionRecord",
      name: "TransactionRecord",
      component: TransactionRecord
    },
    {
      path: "/Transaction",
      name: "Transaction",
      component: Transaction
    },
    {
      path: "/Billing",
      name: "Billing",
      component: Billing
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    }
  ]
});
