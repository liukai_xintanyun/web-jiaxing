module.exports = {
  plugins: {
    autoprefixer: {},
    "postcss-px-to-viewport": {
      viewportWidth: 750,
      viewportUnit: "vmin",
      exclude: [/node_modules/]
    }
  }
};
