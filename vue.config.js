module.exports = {
  productionSourceMap: false,
  devServer: {
    proxy: {
      "/api": {
        target: "http://39.96.205.244:8080",
        pathRewrite: {
          "^/api": ""
        }
        // changeOrigin: true
      }
    }
  }
};
